/*
 * Copyright 2019-2023 The Polypheny Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.polypheny.security.mapper;

import lombok.Getter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A class for implementing schema mapping
 * JSON files are used to store the mapping rules
 * JSON files are located in a directory called mapper-files located in the current directory
 */
public class SchemaMapper {
    private static SchemaMapper instance;
    @Getter
    Map<String, ArrayList<String>> mapperTables = new HashMap<>();
    @Getter
    Map<String, Map<String, String>> mapperAttributes = new HashMap<>();
    @Getter
    Map<String, Map<String, String>> mapperAttributesTypes = new HashMap<>();

    public static SchemaMapper getInstance() {
        synchronized (SchemaMapper.class) {
            if (instance == null) {
                instance = new SchemaMapper();
            }
            return instance;
        }
    }

    private SchemaMapper() {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("./mapper-files/mapper.json")) {
            Object parser = jsonParser.parse(reader);
            JSONArray sources = (JSONArray) parser;
            //Iterate over source array
            sources.forEach(item1 -> {
                ArrayList<String> liste = new ArrayList<>();
                JSONObject source = (JSONObject) item1;
                JSONArray tables = (JSONArray) source.get("mapping_tables");
                //Iterate over table array
                tables.forEach(item2 -> {
                    JSONObject table = (JSONObject) item2;
                    JSONArray attributes = (JSONArray) table.get("attributes");
                    String tableName = (String) table.get("table");
                    HashMap<String, String> attrs = new HashMap<>();
                    HashMap<String, String> types = new HashMap<>();
                    //Iterate over attribute array
                    attributes.forEach(item3 -> {
                        try {
                            JSONObject attribute = (JSONObject) item3;
                            String attr = (String) attribute.get("global_mapping");
                            String mapping = (String) attribute.get("local_mapping");
                            String type = (String) attribute.get("type");
                            types.put(attr.toLowerCase(), type);
                            attrs.put(attr.toLowerCase(), mapping);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    mapperAttributesTypes.put(tableName.toLowerCase(), types);
                    mapperAttributes.put(tableName.toLowerCase(), attrs);
                    liste.add((String) table.get("table"));
                });
                mapperTables.put(((String) source.get("source")).toLowerCase(), liste);
            });
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public String getAttributeMapping(String table, String attribute) {
        return mapperAttributes.get(table).get(attribute);
    }

    public String getAttributeTypeMapping(String table, String attribute) {
        return mapperAttributesTypes.get(table).get(attribute);
    }
}
