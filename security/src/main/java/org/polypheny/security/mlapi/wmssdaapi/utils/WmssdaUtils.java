/*
 * Copyright 2019-2023 The Polypheny Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.polypheny.security.mlapi.wmssdaapi.utils;

import com.google.gson.Gson;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.GatewayTimeoutResponse;
import org.polypheny.security.mlapi.wmssdaapi.dto.WmssdaApiResult;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class WmssdaUtils {
    public static WmssdaApiResult callMlApi(String[][] results, String domain_t) {
        HashMap<String, String> mapping = new HashMap<>();
        mapping.put("3", "1");
        mapping.put("4", "2");
        mapping.put("6", "3");
        mapping.put("8", "4");
        mapping.put("12", "5");
        var data = results[0];
        if (data.length == 17 && domain_t != null) {
            int cpt = 17;
            StringBuilder builder = new StringBuilder();
            builder.append("{ \"domain_t\":\"").append(mapping.get(domain_t)).append("\",");
            builder.append("\"data\": [");
            for (String item : data) {
                builder.append("\"").append(item).append("\"");
                if (cpt > 1) {
                    builder.append(",");
                    cpt--;
                }
            }
            builder.append(" ] }");
            final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).connectTimeout(Duration.ofSeconds(20)).build();
            HttpRequest httpRequest = HttpRequest.newBuilder().POST(HttpRequest.BodyPublishers.ofString(builder.toString())).uri(URI.create("http://20.160.28.136:8889/predict")).setHeader("Content-Type", "application/json").build();
            CompletableFuture<HttpResponse<String>> httpResponse;
            httpResponse = httpClient.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString());
            String resultBody = null;
            int resultStatusCode = 0;
            try {
                resultBody = httpResponse.thenApply(HttpResponse::body).get(20, TimeUnit.SECONDS);
                httpResponse.join();
                resultStatusCode = httpResponse.thenApply(HttpResponse::statusCode).get(20, TimeUnit.SECONDS);
                if (resultStatusCode == 200) {
                    Gson gson = new Gson();
                    WmssdaApiResult result = gson.fromJson(resultBody, WmssdaApiResult.class);
                    return result;
                } else {
                    throw new BadRequestResponse("Invalid domain");
                }
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                throw new GatewayTimeoutResponse("ML API is unreachable");
            }
        } else {
            throw new BadRequestResponse("There is no specified domain or there are not enough specified attributes");
        }
    }
}
