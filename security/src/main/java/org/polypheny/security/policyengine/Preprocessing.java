/*
 * Copyright 2019-2023 The Polypheny Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.polypheny.security.policyengine;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.polypheny.security.authentication.model.User;
import org.polypheny.security.policyengine.dto.SelectPolicy;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class for preprocessing a polySQL query
 * Access control policies are stored in json files
 * The json files are located on the directory policy-files in the folder policy-files
 */
public class Preprocessing {
    private static Preprocessing instance;
    private List<SelectPolicy> selectPolicies = new ArrayList<>();

    private Preprocessing() {
        // Read data from json files
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("./policy-files/dml_policies.json")) {
            Object parser = jsonParser.parse(reader);
            JSONArray rules = (JSONArray) parser;
            //Iterate over source array
            rules.forEach(item -> {
                ArrayList<String> granularityRole = new ArrayList<>();
                ArrayList<String> granularityUser = new ArrayList<>();
                JSONObject rule = (JSONObject) item;
                String ruleType = (String) rule.get("rule_type");
                // Get granularity for each rule
                JSONArray ruleGranularityRole = (JSONArray) rule.get("rule_granularity_role");
                JSONArray ruleGranularityUser = (JSONArray) rule.get("rule_granularity_user");
                if (ruleGranularityRole != null) {
                    ruleGranularityRole.forEach(item2 -> {
                        String role = (String) item2;
                        granularityRole.add(role);
                    });
                }
                if (ruleGranularityUser != null) {
                    ruleGranularityUser.forEach(item2 -> {
                        String user = (String) item2;
                        granularityUser.add(user);
                    });
                }
                if (ruleType != null) {
                    switch (ruleType) {
                        case "select":
                            ArrayList<String> fselect = new ArrayList<>();
                            JSONArray forbiddenSelect = (JSONArray) rule.get("forbidden_select");
                            if (forbiddenSelect != null) {
                                forbiddenSelect.forEach(item2 -> {
                                    String select = (String) item2;
                                    fselect.add(select);
                                });
                                SelectPolicy policy = new SelectPolicy(granularityUser, fselect, granularityRole);
                                selectPolicies.add(policy);
                            }
                            break;
                    }
                }
            });
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    // Singleton instance
    public static Preprocessing getInstance() {
        synchronized (Preprocessing.class) {
            if (instance == null) {
                instance = new Preprocessing();
            }
            return instance;
        }
    }

    public boolean validateQuery(ArrayList<String> from, User user) {
        return validateSelectRules(from, user);
    }

    private boolean validateSelectRules(ArrayList<String> from, User user) {
        for (int i = 0; i < selectPolicies.size(); i++) {
            SelectPolicy policy = selectPolicies.get(i);
            // check if the user is concerned by this policy
            // Check by role
            boolean role_is_concerned = false;
            if (policy.getRule_granularity_role().contains("*")) {
                role_is_concerned = true;
            } else {
                if (policy.getRule_granularity_role() != null) {
                    var userRoles = user.getRoles().toString();
                    for (String item : policy.getRule_granularity_role()) {
                        if (userRoles.contains(item)) {
                            role_is_concerned = true;
                            break;
                        }
                    }
                }
            }
            // Check by username
            boolean user_is_concerned = false;
            var granularity_user = policy.getRule_granularity_user();
            if (granularity_user != null) {
                user_is_concerned = policy.getRule_granularity_user().contains(user.getUsername()) || policy.getRule_granularity_user().contains("*");
            }

            // check if the query violates select policies
            if (role_is_concerned || user_is_concerned) {
                for (String item : from) {
                    if (policy.getForbidden_select().contains(item)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
