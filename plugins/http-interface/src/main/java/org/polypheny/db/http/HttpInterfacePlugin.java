/*
 * Copyright 2019-2023 The Polypheny Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.polypheny.db.http;


import com.google.common.collect.ImmutableList;
import com.google.gson.JsonSyntaxException;
import io.javalin.Javalin;
import io.javalin.http.Handler;
import io.javalin.http.*;
import io.javalin.plugin.json.JsonMapper;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.jetbrains.annotations.NotNull;
import org.pf4j.Extension;
import org.pf4j.Plugin;
import org.pf4j.PluginWrapper;
import org.polypheny.db.StatusService;
import org.polypheny.db.catalog.Catalog;
import org.polypheny.db.iface.AuthenticationException;
import org.polypheny.db.iface.Authenticator;
import org.polypheny.db.iface.QueryInterface;
import org.polypheny.db.iface.QueryInterfaceManager;
import org.polypheny.db.information.InformationGroup;
import org.polypheny.db.information.InformationManager;
import org.polypheny.db.information.InformationPage;
import org.polypheny.db.information.InformationTable;
import org.polypheny.db.languages.LanguageManager;
import org.polypheny.db.languages.QueryLanguage;
import org.polypheny.db.transaction.TransactionManager;
import org.polypheny.db.util.Util;
import org.polypheny.db.webui.Crud;
import org.polypheny.db.webui.HttpServer;
import org.polypheny.db.webui.crud.LanguageCrud;
import org.polypheny.db.webui.models.Result;
import org.polypheny.db.webui.models.SimplifiedResult;
import org.polypheny.db.webui.models.requests.QueryRequest;
import org.polypheny.security.authentication.AuthenticatorDb;
import org.polypheny.security.authentication.admin.CrudManager;
import org.polypheny.security.authentication.admin.dto.AddRoleRequest;
import org.polypheny.security.authentication.admin.dto.AddUserRequest;
import org.polypheny.security.authentication.dto.AuthenticationRequest;
import org.polypheny.security.authentication.model.User;
import org.polypheny.security.mapper.SchemaMapper;
import org.polypheny.security.mapper.model.*;
import org.polypheny.security.mlapi.cmapi.utils.CmUtils;
import org.polypheny.security.mlapi.dto.VmssdaAndCmResult;
import org.polypheny.security.mlapi.wmssdaapi.dto.WmssdaApiResult;
import org.polypheny.security.mlapi.wmssdaapi.dto.WmssdaResult;
import org.polypheny.security.mlapi.wmssdaapi.utils.WmssdaUtils;
import org.polypheny.security.policyengine.PolicyEngine;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class HttpInterfacePlugin extends Plugin {

    /**
     * Constructor to be used by plugin manager for plugin instantiation.
     * Your plugins have to provide constructor with this exact signature to be successfully loaded by manager.
     */
    public HttpInterfacePlugin(PluginWrapper wrapper) {
        super(wrapper);
    }


    @Override
    public void start() {
        // Add HTTP interface
        Map<String, String> httpSettings = new HashMap<>();
        httpSettings.put("port", "13137");
        httpSettings.put("maxUploadSizeMb", "10000");
        QueryInterfaceManager.addInterfaceType("http", HttpInterface.class, httpSettings);
    }


    @Override
    public void stop() {
        QueryInterfaceManager.removeInterfaceType(HttpInterface.class);
    }


    @Slf4j
    @Extension
    public static class HttpInterface extends QueryInterface {

        @SuppressWarnings("WeakerAccess")
        public static final String INTERFACE_NAME = "HTTP Interface";
        @SuppressWarnings("WeakerAccess")
        public static final String INTERFACE_DESCRIPTION = "HTTP-based query interface, which supports all available languages via specific routes.";
        @SuppressWarnings("WeakerAccess")
        public static final List<QueryInterfaceSetting> AVAILABLE_SETTINGS = ImmutableList.of(new QueryInterfaceSettingInteger("port", false, true, false, 13137), new QueryInterfaceSettingInteger("maxUploadSizeMb", false, true, true, 10000));
        private final ConcurrentHashMap<String, Set<String>> sessionXids = new ConcurrentHashMap<>();
        private final int port;
        private final String uniqueName;
        // Counters
        private final Map<QueryLanguage, AtomicLong> statementCounters = new HashMap<>();
        private final MonitoringPage monitoringPage;
        private static Javalin server;

        // Add support for authentication
        private final AuthenticatorDb authenticator = new AuthenticatorDb();

        //Add support for admin management
        private final CrudManager crudManager = new CrudManager();

        // Add support for control access policies
        private final PolicyEngine engine = new PolicyEngine();

        // Add support for schema mapping
        private final SchemaMapper mapper = SchemaMapper.getInstance();

        public HttpInterface(TransactionManager transactionManager, Authenticator authenticator, int ifaceId, String uniqueName, Map<String, String> settings) {
            super(transactionManager, authenticator, ifaceId, uniqueName, settings, true, false);
            this.uniqueName = uniqueName;
            this.port = Integer.parseInt(settings.get("port"));
            if (!Util.checkIfPortIsAvailable(port)) {
                // Port is already in use
                throw new RuntimeException("Unable to start " + INTERFACE_NAME + " on port " + port + "! The port is already in use.");
            }
            // Add information page
            monitoringPage = new MonitoringPage();
        }

        @Override
        public void run() {
            JsonMapper gsonMapper = new JsonMapper() {

                @NotNull
                @Override
                public <T> T fromJsonString(@NotNull String json, @NotNull Class<T> targetType) {
                    return HttpServer.gson.fromJson(json, targetType);
                }

                @NotNull
                @Override
                public String toJsonString(@NotNull Object obj) {
                    return HttpServer.gson.toJson(obj);
                }

            };
            // Create Https server
            server = Javalin.create(config -> {
                config.jsonMapper(gsonMapper);
                config.enableCorsForAllOrigins();
                config.server(() -> {
                    // Access keystore for self-signed certificat
                    Path keystorePath = Paths.get("./certificate/polystore-keystore.p12");
                    if (!Files.exists(keystorePath)) try {
                        throw new FileNotFoundException(keystorePath.toString());
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                    // Add support for SSL encryption
                    // HTTP Configuration
                    HttpConfiguration httpConfig = new HttpConfiguration();
                    httpConfig.setSecureScheme("https");
                    httpConfig.setSendServerVersion(true);
                    httpConfig.setSendDateHeader(true);
                    HttpConfiguration httpsConfig = new HttpConfiguration(httpConfig);
                    httpsConfig.addCustomizer(new SecureRequestCustomizer());
                    // SSL factory
                    SslContextFactory sslContextFactory = new SslContextFactory.Server();
                    sslContextFactory.setKeyStorePath(keystorePath.toString());
                    sslContextFactory.setKeyStorePassword(System.getenv("KEY_STORE"));
                    sslContextFactory.setTrustStorePath(keystorePath.toString());
                    sslContextFactory.setTrustStorePassword(System.getenv("KEY_STORE"));
                    // Create ssl server
                    Server sslServer = new Server();
                    // Add connectors
                    ServerConnector sslConnector = new ServerConnector(sslServer, new SslConnectionFactory(sslContextFactory, HttpVersion.HTTP_1_1.asString()), new HttpConnectionFactory(httpsConfig));
                    sslConnector.setPort(443);
                    sslServer.setConnectors(new Connector[]{sslConnector});
                    return sslServer;
                });
            }).start();
            server.exception(Exception.class, (e, ctx) -> {
                log.warn("Caught exception in the HTTP interface", e);
                if (e instanceof JsonSyntaxException) {
                    ctx.result("Malformed request: " + e.getCause().getMessage());
                } else {
                    ctx.result("Error: " + e.getMessage());
                }
            });
            // Configure response to be sent in json format
            server.before(("*"), ctx -> {
                ctx.res.setHeader("Content-Type", "application/json");
            });
            /* HTTPS server endpoints */
            // Main page
            server.get("/", main);
            // Validate API Key
            server.before("/dashboard/*", validateApiKey);
            server.before("/query/*", validateApiKey);
            server.before("/auth/*", validateApiKey);
            // Authentication
            server.post("/auth/user/signup", addUser);
            // Query operations
            server.post("/query/sql", ctx -> anyQuery(QueryLanguage.from("sql"), ctx));
            // admin operations
            server.delete("/dashboard/delete_user/{username}", deleteUser);
            server.post("/dashboard/add_role", addRole);
            server.delete("/dashboard/delete_role/{name}", deleteRole);
            StatusService.printInfo(String.format("%s started and is listening on port %d.", INTERFACE_NAME, 443));
        }

        private void addRoute(QueryLanguage language) {
        }

        // Validate API Key
        private final Handler validateApiKey = (ctx -> {
            String key = ctx.req.getHeader("api_key");
            if (key == null || !key.equals(System.getenv("API_KEY"))) {
                throw new UnauthorizedResponse();
            }
        });
        //*****************************************************************
        // API Operations
        //*****************************************************************
        // Admin crud operations
        private final Handler addUser = (ctx -> {
            AddUserRequest request = ctx.bodyAsClass(AddUserRequest.class);
            crudManager.addUser(request);
            ctx.status(201);
        });
        private final Handler deleteUser = (ctx -> {
            String username = ctx.pathParam("username");
            if (username != null && !username.equals("")) {
                crudManager.deleteUser(username);
            } else {
                throw new BadRequestResponse();
            }
            ctx.status(204);
        });
        private final Handler addRole = (ctx -> {
            AddRoleRequest request = ctx.bodyAsClass(AddRoleRequest.class);
            crudManager.addRole(request);
            ctx.status(201);
        });
        private final Handler deleteRole = (ctx -> {
            String name = ctx.pathParam("name");
            if (name != null && !name.equals("")) {
                crudManager.deleteRole(name);
            } else {
                throw new BadRequestResponse();
            }
            ctx.status(204);
        });

        // User read operations
        private User getUser(Context ctx) {
            AuthenticationRequest request = ctx.bodyAsClass(AuthenticationRequest.class);
            try {
                return authenticator.authenticateDomain(request.getUsername(), request.getPassword());
            } catch (AuthenticationException e) {
                throw new NotFoundResponse();
            }
        }

        // Main page of the API
        private final Handler main = ctx -> {
            ctx.res.setHeader("Content-Type", "text/html; charset=UTF-8");
            ctx.result(mainPage());
        };

        private String mainPage() {
            return "<!DOCTYPE html> <html> <head> <title>Welcome to the medical API</title> <meta charset=\"utf-8\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <style> body { font-family: Arial, sans-serif; background-color: #f2f2f2; color: #333333; margin: 0; padding: 0; } .container { max-width: 1200px; margin: 0 auto; padding: 20px; } .header { background-color: #0099cc; color: #ffffff; padding: 10px; text-align: center; } h1 { margin-top: 0; } .navbar { background-color: #333333; color: #ffffff; padding: 10px; text-align: center; } .navbar a { color: #ffffff; text-decoration: none; margin: 0 10px; } .navbar a:hover { text-decoration: underline; } .content { background-color: #ffffff; padding: 20px; margin-top: 20px; border: 1px solid #cccccc; border-radius: 5px; box-shadow: 0px 0px 10px #cccccc; } .footer { background-color: #333333; color: #ffffff; padding: 10px; text-align: center; font-size: 12px; } .footer a { color: #ffffff; text-decoration: none; } .footer a:hover { text-decoration: underline; } </style> </head> <body> <div class=\"container\"> <div class=\"header\"> <h1>Polypheny-DB</h1> </div> <div class=\"navbar\"> <a href=\"#\">Home</a> <a href=\"https://polypheny.com/\">Polypheny-DB</a> <a href=\"https://h2020qualitop.liris.cnrs.fr/wordpress/index.php/project/\">QUALITOP</a> </div> <div class=\"content\"> <h2>Welcome to the Medical API</h2> <p>This API provides medical informations.</p> <p>To use this API, you need to sign up and use the appropriate API key.</p> </div> </div> <div class=\"footer\"> <p>Powered by Polypheny-DB &copy; 2023. All rights reserved. </p> </div> </body> </html> ";
        }

        //*****************************************************************
        // Policy Engine Validation
        //*****************************************************************
        // Preprocessing of the query
        private Boolean valideQuery(RequestQuery request, User user) {
            QueryObject query = request.getQuery();
            var from = query.getFrom();
            return engine.getPreprocessing().validateQuery((ArrayList<String>) from, user);
        }

        // Postprocessing of the query
        private Boolean validateResult(RequestQuery request, String[][] data) {
            QueryObject query = request.getQuery();
            var from = query.getFrom();
            return engine.getPostprocessing().validateQuery((ArrayList<String>) from, data);
        }

        //*****************************************************************
        // Processing a query
        //*****************************************************************
        public void anyQuery(QueryLanguage language, final Context ctx) {

            // Check user credentials
            User user = getUser(ctx);
            if (user == null) {
                throw new NotFoundResponse();
            }

            // Map the incoming request to a query
            RequestQuery request = ctx.bodyAsClass(RequestQuery.class);

            // Preprocessing
            if (!user.getRoles().toString().equals("ADMIN")) {
                Boolean valid = valideQuery(request, user);
                if (!valid) {
                    throw new ForbiddenResponse("Access denied");
                }
            }

            if (request.getResult_red() != null && request.getResult_red().size() != 0 && !request.getResult_red().get(0).equals("")) {
                switch (request.getResult_red().size()) {
                    case 1:
                        if (request.getResult_red().get(0).equals("cm")) {
                            var result = CmUtils.callCmApi();
                            ctx.json(result);
                            // Call ML api
                        } else if (request.getResult_red().get(0).equals("wmssda")) {
                            List<Result> results = executeQueryOnPolystore(language, ctx, request);
                            if (results.get(0).getData().length > 0) {
                                var result = WmssdaUtils.callMlApi(results.get(0).getData(), getDomain(request));
                                ctx.json(new WmssdaResult(result));
                            } else {
                                throw new NotFoundResponse("There is no data matching your where criteria");
                            }
                        } else {
                            throw new BadRequestResponse("Invalid query or parameters");
                        }
                        break;
                    case 2:
                        if (request.getResult_red().contains("cm") && request.getResult_red().contains("wmssda")) {
                            var resultCm = CmUtils.callCmApi();
                            var results = executeQueryOnPolystore(language, ctx, request);
                            WmssdaApiResult resultMl = null;
                            if (results.get(0).getData().length > 0) {
                                resultMl = WmssdaUtils.callMlApi(results.get(0).getData(), getDomain(request));
                            }
                            ctx.json(new VmssdaAndCmResult(resultMl, resultCm.getCm()));
                        } else {
                            throw new BadRequestResponse("Invalid query or parameters");
                        }
                        break;
                }
            } else {
                var results = executeQueryOnPolystore(language, ctx, request);
                ctx.json(new SimplifiedResult[]{new SimplifiedResult(results.get(0).getHeader(), results.get(0).getData())});
            }
        }

        // Execute a query on the polystore
        private List<Result> executeQueryOnPolystore(QueryLanguage language, final Context ctx, RequestQuery request) {
            // Building query
            String sql = constructQuery(request);
            System.out.println(sql);
            // get limit clause if exist
            var limit = request.getQuery().getLimit();
            // Execute query
            QueryRequest query = new QueryRequest(sql, false, true, null, null);
            String sessionId = ctx.req.getSession().getId();
            Crud.cleanupOldSession(sessionXids, sessionId);
            List<Result> results;
            if (sql != null) {
                try {
                    results = LanguageCrud.anyQuery(language, null, query, transactionManager, Catalog.defaultUserId, Catalog.defaultDatabaseId, null);
                    if (results.get(0).getError() == null) {
                        // Reduce the number of tuples depending on the limit clause
                        if (limit != null && results.get(0).getData().length > Integer.parseInt(limit)) {
                            results.get(0).setData(getLinesInRange(results.get(0).getData(), Integer.parseInt(limit)));
                        }
                        // Postprocessing
                        if (!validateResult(request, results.get(0).getData())) {
                            throw new ForbiddenResponse("Access denied");
                        }
                    } else {
                        // If there is an error during execution
                        throw new BadRequestResponse("Invalid query or parameters");
                    }
                } catch (Exception e) {
                    // If there is an error during execution
                    throw new BadRequestResponse("Invalid query or parameters");
                }
            } else {
                throw new BadRequestResponse("Invalid data sources");
            }
            if (!statementCounters.containsKey(language)) {
                statementCounters.put(language, new AtomicLong());
            }
            statementCounters.get(language).incrementAndGet();
            // is empty from cleanupOldInfoAndFiles
            sessionXids.put(sessionId, results.stream().map(Result::getXid).filter(Objects::nonNull).collect(Collectors.toSet()));
            return results;
        }

        // Build query from request
        private String constructQuery(RequestQuery request) {
            String[] NUMERIC_TYPES = {"BIGINT", "DECIMAL", "REAL", "SMALLINT", "INTEGER", "TINYINT", "DOUBLE", "BOOLEAN", "DATE", "TIME", "TIMESTAMP"};
            // Get all necessary information about the query
            QueryObject query = request.getQuery();
            var from = query.getFrom();
            var attributes = query.getAttributes();
            var limit = query.getLimit();
            var order_by = query.getOrder_by();
            var group_by = query.getGroup_by();
            var aggregate = query.getAggregate();
            var where = query.getWhere();
            // Start building the query from the information provided by the request
            StringBuilder builder = new StringBuilder();
            builder.append("SELECT * FROM (");
            for (String item : from) {
                StringBuilder builderSubQuery = new StringBuilder();
                var tables = mapper.getMapperTables().get(item);
                if (tables == null || tables.size() == 0) {
                    return null;
                }
                int nbTables = tables.size();
                for (String table : tables) {
                    builderSubQuery.append("SELECT ");
                    int nbAttributes = attributes.size();
                    // build the attribute part
                    for (String attribute : attributes) {
                        var attributeNameFromTable = mapper.getAttributeMapping(table.toLowerCase(), attribute.toLowerCase());
                        if (attributeNameFromTable != null) {
                            builderSubQuery.append(attributeNameFromTable).append(" AS ").append(attribute);
                        } else {
                            if (Arrays.asList(NUMERIC_TYPES).contains(mapper.getAttributeTypeMapping(table.toLowerCase(), attribute.toLowerCase()))) {
                                builderSubQuery.append("-1").append(" AS ").append(attribute);
                            } else {
                                builderSubQuery.append("'-1'").append(" AS ").append(attribute);
                            }
                        }
                        if (nbAttributes > 1) {
                            builderSubQuery.append(" , ");
                            nbAttributes--;
                        }
                    }
                    // build the table part
                    builderSubQuery.append(" FROM ").append(table);
                    // build the where part
                    int nbWhereConditions = where.size();
                    if (nbWhereConditions > 0) {
                        builderSubQuery.append(" WHERE ");
                        for (WhereObject wh : where) {
                            var attributeSubQuery = mapper.getAttributeMapping(table.toLowerCase(), wh.getAttribute().toLowerCase());
                            if (attributeSubQuery == null) {
                                builderSubQuery.setLength(0);
                                nbTables--;
                                continue;
                            }
                            builderSubQuery.append(attributeSubQuery.toLowerCase() + " " + wh.getOperator().toLowerCase() + " " + wh.getValue().toLowerCase());
                            if (nbWhereConditions > 1) {
                                builderSubQuery.append(" and ");
                                nbWhereConditions--;
                            }
                        }
                    }
                    // Add the union all operator if there are multiple data sources
                    if (nbTables > 1) {
                        builderSubQuery.append(" UNION ALL ");
                        nbTables--;
                    }
                }
                builder.append(builderSubQuery);
            }
            builder.append(")");
            // build the aggregation part
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append(" ");
            if (aggregate.size() > 0) {
                int nbAggregateOperations = aggregate.size();
                queryBuilder.append("SELECT ");
                for (AggregateObject item : aggregate) {
                    queryBuilder.append(item.getOperation());
                    queryBuilder.append("(");
                    queryBuilder.append(item.getAttribute());
                    queryBuilder.append(")");
                    queryBuilder.append(" as ").append(item.getAttribute()).append(" ");
                    if (nbAggregateOperations > 1) {
                        queryBuilder.append(" , ");
                        nbAggregateOperations--;
                    }
                }
                queryBuilder.append("FROM(");
            } else {
                queryBuilder.append("SELECT * FROM (");
            }
            queryBuilder.append(builder);
            queryBuilder.append(")");
            // build the group by part
            if (group_by.size() > 0) {
                int nbAttributesGroupBy = group_by.size();
                queryBuilder.append(" group by ");
                for (String item : group_by) {
                    queryBuilder.append(item);
                    if (nbAttributesGroupBy > 1) {
                        queryBuilder.append(" , ");
                        nbAttributesGroupBy--;
                    }
                }
            }
            // build the order by part
            if (order_by.size() > 0) {
                int nbAttributesOrderBy = order_by.size();
                queryBuilder.append(" order by ");
                for (OrderBy item : order_by) {
                    queryBuilder.append(item.getAttribute()).append(" ").append(item.getOption());
                    if (nbAttributesOrderBy > 1) {
                        queryBuilder.append(" , ");
                        nbAttributesOrderBy--;
                    }
                }
            }
            return queryBuilder.toString();
        }

        // Reduce the number of tuples if a limit clause is specified
        private String[][] getLinesInRange(String[][] lines, int limit) {
            String[][] result = new String[limit][];
            System.arraycopy(lines, 0, result, 0, limit);
            return result;
        }

        private String getDomain(RequestQuery requestQuery) {
            var where = requestQuery.getQuery().getWhere();
            for (var item : where) {
                if (item.getAttribute().equalsIgnoreCase("medical_unit")) {
                    return item.getValue();
                }
            }
            return null;
        }

        //*****************************************************************
        // Polypheny-DB developer's code
        //*****************************************************************
        @Override
        public List<QueryInterfaceSetting> getAvailableSettings() {
            return AVAILABLE_SETTINGS;
        }

        @Override
        public void shutdown() {
            server.stop();
            monitoringPage.remove();
        }

        @Override
        public String getInterfaceType() {
            return "Http Interface";
        }

        @Override
        protected void reloadSettings(List<String> updatedSettings) {

        }

        @Override
        public void languageChange() {
            for (QueryLanguage language : LanguageManager.getLanguages()) {
                addRoute(language);
            }
        }

        private class MonitoringPage {

            private final InformationPage informationPage;
            private final InformationGroup informationGroupRequests;
            private final InformationTable statementsTable;

            public MonitoringPage() {
                InformationManager im = InformationManager.getInstance();

                informationPage = new InformationPage(uniqueName, INTERFACE_NAME).fullWidth().setLabel("Interfaces");
                informationGroupRequests = new InformationGroup(informationPage, "Requests");

                im.addPage(informationPage);
                im.addGroup(informationGroupRequests);

                statementsTable = new InformationTable(informationGroupRequests, Arrays.asList("Language", "Percent", "Absolute"));
                statementsTable.setOrder(2);
                im.registerInformation(statementsTable);

                informationGroupRequests.setRefreshFunction(this::update);
            }

            public void update() {
                double total = 0;
                for (AtomicLong counter : statementCounters.values()) {
                    total += counter.get();
                }

                DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
                symbols.setDecimalSeparator('.');
                DecimalFormat df = new DecimalFormat("0.0", symbols);
                statementsTable.reset();
                for (Map.Entry<QueryLanguage, AtomicLong> entry : statementCounters.entrySet()) {
                    statementsTable.addRow(entry.getKey().getSerializedName(), df.format(total == 0 ? 0 : (entry.getValue().longValue() / total) * 100) + " %", entry.getValue().longValue());
                }
            }

            public void remove() {
                InformationManager im = InformationManager.getInstance();
                im.removeInformation(statementsTable);
                im.removeGroup(informationGroupRequests);
                im.removePage(informationPage);
            }

        }

    }

}