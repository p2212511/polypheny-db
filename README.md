# I. Introduction

## 1. Motivation

Nowadays, data-intensive applications have to deal with the problem of handling heterogeneous data. Since no platform is capable of supporting everything, various stores (RDBMS, NewSQL, NoSQL) for different workloads and use cases have been developed and each store has its technology and characteristics.

As a result, the process of integrating and sharing data between each store has become a very difficult challenge, and scientists and developers are trying to find a solution that combines consistency and availability.
One of these solutions is called **polystore**.

**Polystores systems** try to access different stores transparently and combine their capabilities to achieve one or multiple given use cases.

### 1.1. What is a polystore ?

A **polystore system** is a type of database management system that allows different data models to be stored and accessed within a single database.

In traditional databases, data is typically stored in a single format, such as a relational model. In contrast, a **polystore system** supports multiple data models, such as graph, document, key-value, or relational models, among others.

This flexibility enables organizations to store and manage diverse data types efficiently, making it easier to integrate data from multiple sources and support a wide range of applications. For instance, a **polystore system** can store and manage structured data such as customer information, unstructured data like social media feeds, and semi-structured data such as sensor data from IoT devices, all in one place.

**Polystores** are capable of achieving autonomous cost-based optimization and adaption of the database system at runtime to achieve both: high performance under high workload and low resource costs in times of low workload.

The **polyglot persistence philosophy** can be summarized in the following figure :

<div style="text-align: center;">
    <img width='50%' alt="The polyglot persistence philosophy" src="documentation/polyglot.png">
</div>

### 1.2. What is QUALITOP ?

<div style="text-align: center;">
    <img width='50%' alt="QUALITOP logo" src="documentation/qualitop-logo.png">
</div>

**Cancer immunotherapy** brought about significant progress in cancer treatment.

But two main challenges still impede improving cancer patients’ health status and quality of life after immunotherapy initiation:

1. A crucial need for predictive markers of the occurrence of immunotherapy-related adverse events (IR-AEs) to predict and improve patients’ health status and promote their quality of life.
2. The lack of knowledge on patients after the start of immunotherapy outside randomized controlled trials.

**QUALITOP** aims at developing a European immunotherapy-specific open Smart Digital Platform and using big data analysis, artificial intelligence, and simulation modeling approaches. To collect and aggregate efficiently real-world data to monitor the health status and quality of life of cancer patients given immunotherapy.

## 2. Problem Statement

**Data sharing** is the practice of providing partners with access to information that they cannot access in their data systems. This allows stakeholders to learn from each other and collaborate on shared priorities.

The partners participating in the **QUALITOP** project want to benefit from a wide range of locally collected data from multiple sites.

But, the publication and sharing of these data, coming from multiple sources, raises serious concerns in terms of security. Patients want to believe that the use and manipulation of their information are secure and that sensitive information is not exposed.

In this context, each partner must follow a data management policy (charter of the hospital and the country's legislation), which makes the sharing process almost impossible.

**If no guarantees are provided by the system, the project ends up in a deadblock situation.**

## 3. Objectives

The main purpose of this work is to :

1. Propose and design a mechanism that can provide access to the databases of all project stakeholders regardless of the **structure**, **model**, and **type** of data.
2. Find a way to implement **access control policies** to ensure that the process of sharing data between the stakeholders is secure and sensitive information is not exposed.

## 4. Context

This work is done as a part of "[UE - Ouverture à la recherche](https://offre-de-formations.univ-lyon1.fr/%2Fue-26121-1609%2Fouverture-a-la-recherche.html)" module of the first-year Master's degree in Computer Science at the Department of Computer Science at Claude Bernard University Lyon 1 and the [The QUALITOP project](https://h2020qualitop.liris.cnrs.fr/wordpress/index.php/project/).

This work is supervised by **[Mohand-Saïd Hacid](https://liris.cnrs.fr/page-membre/mohand-said-hacid)** and **[Juba AGOUN](https://liris.cnrs.fr/page-membre/juba-agoun)**.

The core of the solution is **[Polypheny-DB](https://polypheny.com/)** wich is a polystore system.

At the end of this work, the proposed solution will be materialized in the form of a module that will be made available to the community with appropriate documentation.

## 5. PolyPheny-DB

**Polypheny-DB** is a database management system (DBMS) that is designed to support the efficient processing of complex queries over large and heterogeneous datasets. As a polystore system, it uses highly optimized databases and utilizes them as execution engines.

**Polypheny-DB** is a powerful and flexible DBMS that is well-suited for a wide range of data-intensive applications, including data analytics, machine learning, and more. Furthermore, it is an extremely useful tool for data scientists.

These are some the benefits of using **Polypheny-DB**:

1. One single database instance to manage.
2. A wide variety of underlying database backends.
3. Simple replacement of databases in existing application scenarios.
4. Tighter integration of data between different applications.
5. Support for multiple data models and cross-model queries.
6. Modern user interface.
7. Runs on Windows, Linux and macOS.

Polypheny-DB offers very various adapters for integrating data. For example :

<div style="text-align: center;">
    <img width='95%' alt="Some of the adapters provided by polypheny-DB" src="documentation/adapters-asset.png">
</div>

> Note : **User Authentication** and **User Authorization** are not provided with **polypheny-DB** yet.

> Note: There is no mechanism for implementing **access control policies**.

This work will focus on adding a **security layer** (Authentication + Authorization) to **polypheny-DB**, as it will lead us to achieve our objectives. We will also add a **mechanism to implement access control policies**.

The source code of **Polypheny-DB** is available in open source format under the following repository: [https://github.com/polypheny/Polypheny-DB](https://github.com/polypheny/Polypheny-DB)

For more details, please refer to **Polypheny-DB website**: ["https://polypheny.com/en/"](https://polypheny.com/en/)

## 6. Summary

The following figure shows the initial architecture of the solution :

<div style="text-align: center;">
    <img  alt="The initial architecture of the solution" src="documentation/initial-architecture.png">
</div>

The following figure shows the final architecture of the solution :

<div style="text-align: center;">
    <img  alt="The final architecture of the solution" src="documentation/final-architecture.png">
</div>

# III. Methodology

## 1. State of the art

The first step was bibliographic research to become familiar with everything related to **polystore systems**, **security**, **data sharing**, **data integration**, **control access policies** and **distributed databases**.

The following list enumerates all the articles used to build this work :

1. [Introduction to data sharing and integration](https://aisp.upenn.edu/wp-content/uploads/2020/06/AISP-Intro-.pdf)
2. [Polystore Systems for Data Integration of Large Scale Astronomy Data Archives](https://u-aizu.repo.nii.ac.jp/?action=repository_action_common_download&item_id=220&item_no=1&attribute_id=20&file_no=1)
3. [Polypheny-DB: Towards a Distributed and Self-Adaptive Polystore](https://dbis.dmi.unibas.ch/publications/2018/scdm18/polypheny-db.pdf)
4. [Towards Polyglot Data Stores](https://arxiv.org/pdf/2204.05779.pdf)
5. [Access Control 101: A Comprehensive Guide to Database Access Control](https://satoricyber.com/access-control/access-control-101-a-comprehensive-guide-to-database-access-control/)
6. [Security Issues in Distributed Database System Model]()
7. [Polypheny-DB: Towards Bridging the Gap Between Polystores and HTAP Systems](https://link.springer.com/chapter/10.1007/978-3-030-71055-2_2)

## 2. Deploy Polypheny-DB locally

The second step was to download the source code of **Polypheny-DB**.

The source code is open source and available at the following link : [https://github.com/polypheny/Polypheny-DB](https://github.com/polypheny/Polypheny-DB)

Since the polystore is written using java programming language, we open the code base using a java IDE (For this work we use [IntelliJ IDEA](https://www.jetbrains.com/fr-fr/idea/)).

The first launch may take a long time since the project needs to download all the necessary dependencies. When the download is done, the **polystore user interface** launch in port **8080**.

## 3. Code analysis

To understand the internal architecture of the polystore, a deep code analysis is needed.

After the code analysis, we were able to draw the following architecture:

<div style="text-align: center;">
    <img  alt="Polypheny-DB internal architecture" src="documentation/internal-architecture.png">
</div>

### 3.1. Internal modules

1. **Query Interface** : A query is received through the query interface and forwarded to the matching query parser.
2. **Query parser** : The parser translates the query into a logical query plan. This plan is represented as a tree of relational operators based on an extended relational algebra.
3. **Query Execution** : This module uses 3 other modules : **Locking module**, **Routing module** and **Optimization module**.
4. **Locking module** : This module processes the logical query plan to guarantee the isolation of transactions.
5. **Routing module** : When all locks are acquired, this module decides on which of the underlying data stores or sources the query should be executed.
6. **Optimization module** : This module optimizes and translates the routed query plan into a physical query plan. This physical query plan contains implementations of the relational operators.

### 3.2. Databases

**Polypheny-DB** supports two types of underlying databases: **Sources** and **Stores**.

**Sources** are managed externally and can directly be attached to the polystore.

**Stores** are under the full and exclusive control of polystore. This gives the polystore the ability to optimize for the workload by replicating and partition data across all data stores. This allows to combine the advantages of the database systems and handling a large variety of data and mixed workloads.

The adapters take care of translating the **physical query plan** produced by the **optimization module** into the native query language of the underlying database.

There can be multiple adapters involved in executing a query.

### 3.3. Entry points

**Polypheny-DB** is built to offer the user multiple ways to interact with it and fetch data. When the polystore is launched, it launches 4 interfaces, each one in a different thread.

1. **HTTP interface** : This interface is launched on port **13137**. It is an HTTP server ([Javlain server](https://javalin.io/)). The user interacts with it by sending an HTTP request to fetch data.
2. **REST interface** : This interface is launched on port **8089**. It is an HTTP server ([Javlain server](https://javalin.io/)). The user interacts with it by sending an HTTP request to fetch data.
3. **AVATICA interface** : This interface is launched on port **20591**. The user interacts with it by using the **[JDBC driver](https://github.com/polypheny/Polypheny-JDBC-Driver/blob/master/CHANGELOG.md)** or the **[Python connector](https://github.com/polypheny/Polypheny-Control/releases/tag/v1.3)** to fetch data.
4. **Polypheny-UI** : This interface is launched on port **8080**. It's a web application. This interface allows the management and monitoring of the Polypheny system and enables the management of the data and the schema and also provides additional methods for querying the data.

## 4. Implementation choices

Since each interface acts as an independent entity, if we want to add a security layer to the polystore, then we must adapt the security layer for each interface, which will take a lot of time.

After discussions, we decide to keep only one interface running which is the **HTTP interface** and shut down all the others. We also decided to change the running port from **13137** to **443** so we can implement **HTTPS protocol** to encrypt the communications between the polystore and the partners.

The polystore will become an **API** which will serve the incoming requests from the partners.

The following figure shows a simplified architecture of the polystore :

<div style="text-align: center;">
    <img  alt="Polypheny-DB simplified architecture" src="documentation/simplified-internal-architecture.png">
</div>

## 5. Redesign of the HTTP interface

Now that we kept only one running interface. We will focus on it, so we can improve it and add our security Layer (User Authentication + User Authorization + Access control policies).

The following figure shows the new redesigned HTTP interface :

<div style="text-align: center;">
    <img  alt="Redesign of the HTTP interface" src="documentation/HttpInterface.png">
</div>

### 5.1. Security Layer

As we mentioned previously, the purpose of this work is to add a security layer to the polystore.

The security layer has 3 main modules:

#### 5.1.1. Authentication

This module is used by the polystore to authenticate users.

To improve security, user passwords are hashed in **SHA-256** format.

To improve performance, this module uses an embedded database that is not accessible from out of the polysore. This approach leads to better performance and decreases the vulnerability of the system.

We use the following schema to model users and roles :

<div style="text-align: center;">
    <img  alt="User + Roles schema" width="70%" src="documentation/users.png">
</div>

#### 5.1.2. Policy Engine

To implement access control policies, we have implemented from scratch a new policy engine.

This engine utilizes **JSON files** to store the policies defined by the administrator. These **json files** are encrypted to enhance security.

We have multiple types of access control policies, These are some of them :

1. **Select policy** : Restricting access to a certain data source. A select policy has the following format :

```sh
 {
    "rule_type": "select", 
    "rule_granularity_role":[],
    "rule_granularity_user": [],
    "forbidden_select": [
    ]
}
```

- **rule_type**: Specifies the type of the rule. Here it's **select**.
- **rule_granularity_role** : Specifies the list of roles that are concerned by this policy. For example, if we specify **["DOCTOR"]** as a value, all the users who have the role **DOCTOR** will be concerned by this policy.
- **rule_granularity_user** : Specifies the list of usernames who are concerned by this policy. For example, if we specify **["doctor@gmail.com","doctor2@gmail.com"]**, then this list of users will be concerned by this policy.
- **forbidden_select** : Specifies the list of forbidden data sources.

2. **Join policy** : Disallow a join query between 2 or more data sources. A Join policy has the following format :

```sh
{
    "rule_type": "join",
    "rule_granularity_role":[],
    "rule_granularity_user": [],
    "forbidden_join": [
    ]
}
```

- **rule_type**: Specifies the type of the rule. Here it's **join**.
- **rule_granularity_role** : Specifies the list of roles that are concerned by this policy. For example, if we specify **["DOCTOR"]**, all the users who have the role **DOCTOR** will be concerned by this policy.
- **rule_granularity_user** : Specifies the list of usernames who are concerned by this policy. For example, if we specify **["doctor@gmail.com","doctor2@gmail.com"]**, then this list of users will be concerned by this policy.
- **forbidden_join** : Specifies the list of data sources that can't be in the same join query.

2. **Tuple policy** : For security purposes, sometimes we don't want a query to return a number of tuples less than a certain value. A Tuple policy has the following format :

```sh
{
    "rule_type": "nb_tuple",
    "rule_granularity_role":[],
    "rule_granularity_user": [],
    "source": "",
    "nb": ""
}
```

- **rule_type**: Specifies the type of the rule. Here it's **nb_tuple**.
- **rule_granularity_role** : Specifies the list of roles that are concerned by this policy. For example, if we specify **["DOCTOR"]**, all the users who have the role **DOCTOR** will be concerned by this policy.
- **rule_granularity_user** : Specifies the list of usernames who are concerned by this policy. For example, if we specify **["doctor@gmail.com","doctor2@gmail.com"]**, then this list of users will be concerned by this policy.
- **source** : Specifies the data source concerned by this policy.
- **nb** : Specifies the number of tuples that the query must at least return. Unless an error is thrown.

#### 5.1.3. Logging module

This module is only for analysis purposes. It stores all input and output events in log files.

### 5.2. HTTP interface

We distinguish 2 types of operations: **User operations** and **Admin operations**.

#### 5.2.1. User operations

When a user sends a **query request** in JSON format to the **HTTP interface**, the following workflow is executed by the polysotre:

1. **User authentication** : Before even building the query, the polystore ensures that the request is coming from an authorized signed user. This can be done by adding 2 attributes to the query request: **username** and **password**. If the user doesn't exist **an error is returned to the user**.
2. **Parser** : The parser converts the **query request** which is in JSON format to a **[PolySQL](https://community.polypheny.org/documentation/PolySQL/)** query which will be executed by the polystore to fetch data. A schema mapping is done to map global attributes to local attributes.
3. **Check global policies** : Before fetching data, the polystore ensures that the query submitted by the user doesn't violate the access control policies defined by the administrator. The polystore calls the Policy engine to analyze the query. If the query violates 1 access control policy **an error is returned to the user**. We call this part **Preprocessing**.
4. **Fetch data** : The polystore executes the query by using its internal modules. It fetches data from all the underlining data sources and returns the union of the tuples. The data sources can be in any format (Relational model, Object-oriented database model, Document model ...).
5. **Check global policies** : When the data is fetched, the polystore again ensures that the result of the execution of the query doesn't violate the access control policies defined by the administrator. The polystore calls the Policy engine to analyze the result. If the result violates 1 access control policy **an error is returned to the user**. We call this part **Post-processing**.
6. **Format the result** : Before sending the result to the user, the polysotre converts the result to a **Query Response** and sends it to the user in **JSON format**.

#### 5.2.2. Admin operations

We gave the administrator the ability to :

1. Add new users to the polystore.
2. Add new roles to the polystore.
3. Add new access control policies.

### 5.3. Integration with Machine Learning APIs

Now that we have redesigned the **HTTP interface** to add the security Layer to PolyPheny-DB. We want to enrich Polypheny-DB, by making it communicate with 2 Machine learning API that was **developed by two PhD students from the laboratory and part of the QUALITOP project**.

The first API is **wmssda**. This API takes a patient as input, and returns probability of **DEATH**.

The second API is **cm**. This API is responsible for **Clustering Longitudinal Data For Patient Subtyping**

After adding the new changes, we have this new architecture:

<div style="text-align: center;">
    <img  alt="Polypheny-DB architecture after adding the communication with machine learning APIs" width="70%" src="documentation/integration-ml.png">
</div>

## 6. Coding

As we mentioned previously, the security layer will be added as a new module to the polystore. We called this module security and it is divided into 4 sub-modules: **authentication**, **policyengine**, **mlapi** and **mapper**.

The following figure illustrates the architecture of the security module:

<div style="text-align: center;">
    <img  alt="Security module" width="30%" src="documentation/security-module.png">
</div>

1. **authentication** : This module contains all the necessary classes and packages for authenticating users.
2. **policyengine** : This module contains all the necessary classes and packages for implementing access control policies.
3. **mlapi** : This module contains all the necessary classes and packages for establishing a connection with the wmssda API and cm API.
4. **mapper** : This module contains all the necessary classes and packages for implementing schema mapping.

## 7. Interaction with the HTTPS interface

### 7.1. SignUp request

A user must sign up into the **Polystore** level, to send query requests.

The **roles** are determined by the authorities of the project. *This should be discussed in relation to how the registration of new users should be developed.*

An **API key** will be used as an alternative for json web tokens. It will be used to ensure that query requests are coming from only authorized applications.*

The polystore solution is accessible through the following url: https://polystore-qualitop.westeurope.cloudapp.azure.com/ or https://20.160.28.136

A **SignUp request** has the following format :

```sh
# Request header
{
    "api_key":"...", # we specify the api key in the header of the request
}
# Request body
{
    "username":"...", # Specify the username 
    "password":"...", # Specify the hashed password in SHA256 format
    "roles":["..."] # Specify the user's roles. Note: Currently, the polystore support only the following roles: DOCTOR, ADMIN, NURSE.
}
```

To register a new user, a request must be sent to this endpoint : https://polystore-qualitop.westeurope.cloudapp.azure.com/auth/user/signup or https://20.160.28.136/auth/user/signup

The query is of type **POST**.

**Example:**

Here is an example of how to add a new user in the polystore:

```sh
# Request header
{
    "api_key":"secret_api_key", 
}
# Request body
{
    "username":"new_user", 
    "password":"0f6f284f586b81b08ef5f77cffe733a0bfdc990bf00402b45fc83fa419944823", 
    "roles":["DOCTOR"] 
}
```

### 7.2. Query request format

A **Query request** is used to fetch data from the polystore. A query request has the following format:

```sh
# Request header
{
    "api_key":"...", # We specify the api key in the header of the request 
}
# Request body
{
    "username":"...", # Here we specify the username for authentication
    "password":"...", # Here we specify the hashed password in SHA256 format for authentication
    "result_red":["..."], # here we specify the result redirection attribute i.e., the option wmssda 
                          # will retrun the dead/alive response of the query, the option "cm" will return the result of the sub types of the alzahaimer data
    "query":
    {
        "attributes":["attribute1","..."],# Here we specify the global attributes to fetch
        "from":["..."], # Here we specify the data source
        "where":
        [
            {
                "attribute":"attribute1",
                "operator":"", # options :  =, >,  >=,  <,  <=, not
                "value":"..." #  if string add '' to the value
            },
           # ...
        ],
        "group_by":["attribute1","..."],
        "order_by":[
                        {
                            "attribute1": "",
                            "option":"" # option : DESC, ACS
                        } 
                   ],
        "limit":"value", # Here we limit the number of tuple returned by the polystore. An empty or null value will lead to fetch all tuples
                         # matching where criteria
        "aggregate":
        [
            {
                "attribute":"...",
                "operation":"..." # options : MAX, COUNT, MIN ...
            },
            #...
        ] 
    }
}
```

Note that the **"from"** attribute is used to indicate the data source from where to fetch the data.

**Roles access policies** can be implemented through the use of **"username"** and **"password"**. When a user submits a request to the polystore, we fetch his roles from the embedded database of the polystore.

The signing up of new users to our polystore will be discussed later since our polystore is set to be at the backend of the platform.

For deployment issues and to improve performance, no tokens will be exchanged between the server and the client, since there was already an authentication happening when the user logs into his profile. We will just use an API key to certificate that the query request is coming from an authorized application.

All the communication between the client and the server is encrypted using **TLS 1.3 protocol**.

The **result_red** attribute is very important, as it will be used to invoke the local APIs ( **wmssda, cm**). An empty value will invoke the tuples returned by the polystore.

The query request must be sent to this endpoint : https://polystore-qualitop.westeurope.cloudapp.azure.com/query/sql or https://20.160.28.136/query/sql

The query is of type **POST**.

### 7.3. Query Response Format

The **Query response** format depends on the value of the **result_red** attribute.

#### Case 1:

If **result_red** has an empty value : **[""]**, the result is the tuples returned by the polystore which match the where criteria.

The result has the following format :

```sh
[
    {
        "header": [
            {
                "name": "attribute1",
                "dataType": "",
                "nullable": false,
                "primary": false,
                "sort": {
                    "direction": "",
                    "sorting": false
                },
                "filter": ""
            },
            ...
        ],
        "data": [
            [
                ...
            ],
            [
                ...
            ]
        ]
    }
]
```

#### Case 2:

If **result_red** has the value of **["wmssda"]**, the polystore return the result provided by the **wmssda** API.

The result has the following format :

```sh
{
    "wmssda": {
        "ALIVE": "...",
        "DEAD": "..."
    }
}
```

#### Case 3:

If **result_red** has the value of **["cm"]**, the polystore return the result provided by the **cm** API.

The result has the following format :

```sh
{
    "cm": "..."
}
```

**Note:**

For the **wmssda** API to work, the **medical_unit** must be specified in the where section, unless a bad request error will be thrown.

Also, the API waits for 17 attributes and the order is very important, so the API can return a correct result.

The list of attributes needed by the API is :

**["USMER","SEX","PATIENT_TYPE","PNEUMONIA","AGE","PREGNANT","DIABETES"
,"COPD","ASTHMA","INMSUPR","HIPERTENSION","OTHER_DISEASE","CARDIOVASCULAR",
"OBESITY","RENAL_CHRONIC","TOBACCO","CLASIFFICATION_FINAL"]**

#### Case 4:

If **result_red** has the value of **["wmssda","cm"]** or **["cm","wmssda"]**, the polystore returns the combination of the result provided by the **wmssda** API and the **cm** API.

The result has the following format :

```sh
{
    "wmssda": {
        "ALIVE": "...",
        "DEAD": "..."
    },
    "cm": "...."
}
```

## 7.4. Visulazing the results

### 7.4.1. WMSSDA

In order to exploit the result of **wmssda** API it could be easy to use the following code to plot the visualization:

```sh
import matplotlib.pyplot as plt

probas = [.064, 1-.064]  # To be replaced with real prediction
classes = ['DEAD', 'ALIVE']
std = .12 # To be defined with statisticians

plt.bar(classes, probas, alpha=.5, yerr=std, capsize=6)
plt.show()

```

### 7.4.2. CM

In order to exploit the result of **cm** API it could be easy to use the following code to plot the visualization:

```sh
import seaborn as sns
import pandas as pd
import json

result = "" # Attach the result as a string
subtypes = pd.read_json(result)    
sns.heatmap(subtypes, annot=True) # return the following visualisation 
```

## 8. Tests

To test the solution, we have connected 3 different **PostgreSQL** databases to the polystore. These databases contains covid data from 3 different European countries:

1. **covid_data_fr** : From **France**. This database contains the medical data of  **340655** patients.
2. **covid_data_es** : From **Spain**. This database contains the medical data of **340661** patients.
3. **covid_data_de** : From **Germany**. This database contains the medical data of **340661** patients.

The following figure shows the test scenario :

<div style="text-align: center;">
    <img  alt="Test scenario" width="70%" src="documentation/test-scenario.png">
</div>

Now, for example , if we cant to fetch **1 patient who smokes, has an age equal to 50, sex equal to 1, and a medical unit equal to 12**.

We can write the follwoing request:

```sh
{
    "username":"new_user", 
    "password":"0f6f284f586b81b08ef5f77cffe733a0bfdc990bf00402b45fc83fa419944823", 
    "result_red":["wmssda"],
    "query":
    {
        "attributes":["USMER","SEX","PATIENT_TYPE","PNEUMONIA","AGE","PREGNANT",
        "DIABETES","COPD","ASTHMA","INMSUPR","HIPERTENSION","OTHER_DISEASE",
        "CARDIOVASCULAR","OBESITY","RENAL_CHRONIC","TOBACCO","CLASIFFICATION_FINAL"],
        "from":["covid_data"],
        "where":
        [
            {
                "attribute":"age",
                "operator":"=",
                "value":"50"
            },
            {
                "attribute":"sex",
                "operator":"=",
                "value":"1"
            },
            {
                "attribute":"TOBACCO",
                "operator":"=",
                "value":"1"   
            },
            {
                "attribute":"medical_unit",
                "operator":"=",
                "value":"12"   
            }
        ],
        "group_by":[
        ],
        "order_by":[
            ],
        "aggregate":[
        ],
        "limit":"1"
    }
}
```

When **result_red** has the value of **[""]**, the result is returned in the following format (JSON format):

```sh
[
    {
        "header": [
            {
                "name": "USMER",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "SEX",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "PATIENT_TYPE",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "PNEUMONIA",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "AGE",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "PREGNANT",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "DIABETES",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "COPD",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "ASTHMA",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "INMSUPR",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "HIPERTENSION",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "OTHER_DISEASE",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "CARDIOVASCULAR",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "OBESITY",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "RENAL_CHRONIC",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "TOBACCO",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            },
            {
                "name": "CLASIFFICATION_FINAL",
                "dataType": "INTEGER",
                "sort": {
                    "direction": "DESC",
                    "sorting": false
                },
                "filter": "",
                "primary": false,
                "nullable": false
            }
        ],
        "data": [
            [
                "1",
                "1",
                "2",
                "1",
                "50",
                "2",
                "2",
                "2",
                "2",
                "2",
                "2",
                "2",
                "2",
                "2",
                "2",
                "1",
                "7"
            ]
        ]
    }
]
```

When **result_red** has the value of **["wmssda"]**, the result is returned in the following format (Json format):

```sh
{
    "wmssda": {
        "ALIVE": "0.23936827",
        "DEAD": "0.76063174"
    }
}
```

When **result_red** has the value of **["cm"]**, the result is returned in the following format (JSON format):

```sh
{
    "cm": "{\"ADAS11\":{\"0\":0.1110915223,\"1\":0.3192348219},\"CDRSB\":{\"0\":0.0509376518,\"1\":0.3135257548},\"RAVLT_immediate\":{\"0\":0.5223927128,\"1\":0.2785936787},\"MMSE\":{\"0\":0.9407314439,\"1\":0.72326614},\"FDG\":{\"0\":0.5318395357,\"1\":0.4593256197},\"Entorhinal\":{\"0\":0.4483969757,\"1\":0.3435715427},\"ADAS13\":{\"0\":0.1469259519,\"1\":0.3825032535},\"Hippocampus\":{\"0\":0.5214073526,\"1\":0.4087201654},\"FAQ\":{\"0\":0.0541916897,\"1\":0.5626254008}}"
}
```

When **result_red** has the value of **["cm","wmssda"]** or **["wmssda","cm"]**, the result is returned in the following format (JSON format):

```sh
{
    "wmssda": {
        "ALIVE": "0.23936827",
        "DEAD": "0.76063174"
    },
    "cm": "{\"ADAS11\":{\"0\":0.1110915223,\"1\":0.3192348219},\"CDRSB\":{\"0\":0.0509376518,\"1\":0.3135257548},\"RAVLT_immediate\":{\"0\":0.5223927128,\"1\":0.2785936787},\"MMSE\":{\"0\":0.9407314439,\"1\":0.72326614},\"FDG\":{\"0\":0.5318395357,\"1\":0.4593256197},\"Entorhinal\":{\"0\":0.4483969757,\"1\":0.3435715427},\"ADAS13\":{\"0\":0.1469259519,\"1\":0.3825032535},\"Hippocampus\":{\"0\":0.5214073526,\"1\":0.4087201654},\"FAQ\":{\"0\":0.0541916897,\"1\":0.5626254008}}"
}
```

# III. Conclusion

**Polystore systems** are still a relatively new concept, they hold great potential as the future of databases. As the volume and complexity of data continue to grow, organizations will need flexible and powerful solutions to manage and analyze it effectively. Polystores offer a promising path forward for achieving this goal.

In this work, we have covered one of these polystores which is **[Polypheny-DB](https://polypheny.com/)** and we proposed a solution for a successful data-sharing between the stakeholders of the **QUALITOP project**.

I want to take a moment to express my gratitude to **[Mohand-Saïd Hacid](https://liris.cnrs.fr/page-membre/mohand-said-hacid)** and **[Juba AGOUN](https://liris.cnrs.fr/page-membre/juba-agoun)**. Thank you for your guidance and support throughout my time working with you. Your leadership and expertise have been invaluable in helping me grow both personally and professionally.

I am grateful for the opportunities you have provided me to learn and develop new skills, and for the trust you have placed in me to take on important projects. Your willingness to provide constructive feedback has allowed me to improve and excel in my work.

Thank you for your unwavering support and for believing in me. I appreciate all that you have done for me and look forward to continuing to learn from you in the future.

# IV. References

[1] [Actionable Intelligence for Social Policy University of Pennsylvania. (2020). Introduction to Data Sharing & Integration.](https://aisp.upenn.edu/wp-content/uploads/2020/06/AISP-Intro-.pdf).

[2] [Dr.C.Sunil Kumar, J.Seetha, S.R.Vinotha, Anna University, Chennai, India  (2012, 11, 25). Security Implications of Distributed Database Management](https://arxiv.org/ftp/arxiv/papers/1401/1401.7733.pdf).

[3] [Johes Bater, Gregory Elliott, Craig Eggen, Satyender Goel, Abel Kho,   Jennie Rogers (2017, 03, 06). smcql: Secure Querying for Federated Databases.](https://vldb.org/pvldb/vol10/p673-rogers.pdf).

[4] [Manoj Poudel, Rashmi P. Sarode , Yutaka Watanobe, Maxim Mozgovoy and Subhash Bhalla (2022, 03, 04). Processing Analytical Queries over Polystore System for a Large Astronomy Data Repository](https://www.mdpi.com/2076-3417/12/5/2663).

[5] [Vijay Gadepally, Kyle O’Brien, Adam Dziedzic, Aaron Elmore, Jeremy Kepner, Samuel Madden† Tim Mattson, Jennie Rogers, Zuohao She, Michael Stonebrake (2017, 06 02). Version 0.1 of the BigDAWG Polystore System.](https://dspace.mit.edu/bitstream/handle/1721.1/137850/1707.00721.pdf?sequence=2&isAllowed=y)

[6] [Yang Cao, Wenfei Fan, Yanghao Wang, Ke Yi (2020). Querying Shared Data with Security Heterogeneity](https://www.pure.ed.ac.uk/ws/portalfiles/portal/143085316/Querying_Shared_Data_CAO_DOA07042020_AFV.pdf).

[7] [MANOJ POUDEL (2022). Polystore Systems for Data Integration of Large Scale Astronomy Data Archives ](https://u-aizu.repo.nii.ac.jp/?action=repository_action_common_download&item_id=220&item_no=1&attribute_id=20&file_no=1).

[8] [Marco Vogt, Alexander Stiemer, Heiko Schuldt (2018). Polypheny-DB: Towards a Distributed and Self-Adaptive Polystore](https://dbis.dmi.unibas.ch/publications/2018/scdm18/polypheny-db.pdf).

[9] [DANIEL GLAKE, FELIX KIEHN, MAREIKE SCHMIDT, FABIAN PANSE and NORBERT RITTER, Universität Hamburg (2022, 04, 12). Towards Polyglot Data Stores](https://arxiv.org/pdf/2204.05779.pdf).

[10] [The QUALITOP project](https://h2020qualitop.liris.cnrs.fr/wordpress/index.php/project/).

[11] [The Polypheny-DB project](https://github.com/polypheny/Polypheny-DB).

[12] [The Polypheny-DB website](https://polypheny.com/).
